! -*- coding: utf-8 -*-
!
! Copyright (C) 2020 Snehal M. Shekatkar <snehal@inferred.co>
! Copyright (C) 2020 Ivano Lodato <ivano.lodato@gmail.com>
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.

Module adi
    implicit none

    contains

    subroutine play_game(num_boxes, num_players, max_attempts, tot_permutations, &
        & tot_used_attempts, pmf)
        implicit none
        integer*8 :: num_boxes, num_players, tot_permutations, incr
        integer*8 :: i, j, ix, attempts, max_attempts, tot_winners
        integer*8 :: tot_used_attempts(tot_permutations), box_opened(num_boxes)
        integer*8 :: box(num_boxes), success(num_players), pmf(0:num_players)
    
!f2py intent(in) :: num_boxes, num_players, max_attempts, tot_permutations
!f2py intent(out) :: tot_used_attempts, pmf

        pmf = 0
        tot_used_attempts = 0
        ! Loop over realizations
        DO ix = 1, tot_permutations
            ! Initial condition :
            ! -1 indicates an empty box. We first put all num_players keys in first 
            ! num_players boxes, and keep the remaining boxes empty. Then we shuffle 
            ! this to get a random initial condition
            box = -1 
            Do i = 1, num_players
                box(i) = i
            Enddo
            ! shuffle
            call random_permute(num_boxes, box)
            !print'(20(I0,1X))',box
    
            success = 0
            ! Start the game
            Do i = 1, num_players
                ! All boxes are closed at the start
                box_opened = 0
                ! If the current box is empty, the next box to be opened is (num_players + incr)
                incr = 1
                ! Player i starts from box i. Henceforth j will always denote the 
                ! index of the current box
                j = i
                box_opened(j) = 1
                attempts = 1
                tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                do while (box(j) /= i .and. attempts < max_attempts)
                    IF (box(j) == -1) then
                        ! The opened box is empty, choose an appropriate box
                        j = num_players + incr
                        incr = incr + 1
                    ELSE
                        j = box(j)
                    ENDIF                                        
                    attempts = attempts + 1
                    tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                enddo
                ! Assign success if the key is located in max_attempts
                IF (box(j) == i) success(i) = 1
            Enddo
            tot_winners = sum(success)
            pmf(tot_winners) = pmf(tot_winners) + 1
        ENDDO
    end subroutine play_game
    !=================================================================
    
    function rand_int(a, b)
        ! Return a random integer between a and b (inclusive) uniformly randomly
        implicit none
        integer*8 :: a, b, rand_int
        real*8 :: r
        
        call random_number(r)
        rand_int = a + Floor((b-a+1)*r)
    
    end function rand_int
    
    !=================================================================
    ! random permutation
    subroutine random_permute(n, x)
        ! Algorithm taken from Knuth, vol2
        implicit none
        integer*8 :: i, j, temp, n, x(n)
    
        DO j = n, 1, -1
            i = rand_int(int(1, 8), j)
            temp = x(i)
            x(i) = x(j)
            x(j) = temp
        ENDDO
    end subroutine random_permute
    
    !=================================================================
End module adi
