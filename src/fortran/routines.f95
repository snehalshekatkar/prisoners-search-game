! -*- coding: utf-8 -*-
!
! Copyright (C) 2020 Snehal M. Shekatkar <snehal@inferred.co>
! Copyright (C) 2020 Ivano Lodato <ivano.lodato@gmail.com>
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.

Module strategies
    implicit none

    contains 

    ! Simulate the game using key-strategy
    subroutine play_game(num_boxes, num_players, max_attempts, offset, incr, &
        & strategy, escape_strategy, initial_choice, switch_prob, be_stupid, & 
        & tot_permutations, tot_used_attempts, pmf)
        implicit none
        character (len = 100) :: strategy, escape_strategy, initial_choice
        integer*8 :: i, j, k, ix, num_boxes, num_players, attempts, max_attempts
        integer*8 :: tot_closed, incr, offset, tot_winners, tot_permutations
        integer*8 :: be_stupid, tot_used_attempts(tot_permutations)
        integer*8 :: boxes(num_boxes), success(num_players), pmf(0:num_players)
        integer*8 :: closed_boxes_queue(num_boxes), box_opened(num_boxes)
        real*8 :: switch_prob, r

!f2py   intent(in) :: num_boxes, num_players, max_attempts, incr, be_stupid
!f2py   intent(in) :: strategy, escape_strategy, initial_choice, offset
!f2py   intent(out) :: tot_used_attempts, pmf

        pmf = 0
        tot_used_attempts = 0
        DO ix = 1, tot_permutations
            ! Initial condition :
            ! -1 indicates an empty box. We first put all num_players keys in 
            ! first num_players boxes, and keep the remaining boxes empty. Then 
            ! we shuffle this to get a random initial condition
            boxes = -1 
            DO i = 1, num_players
                boxes(i) = i
            ENDDO
            ! Shuffle
            call random_permute(num_boxes, boxes)
            ! success stores the success value (1 or 0) for each player
            success = 0
            DO i = 1, num_players ! Loop over players
                ! Box_opened is a binary array. An element of the array is 1
                ! if the corresponding box is open, and 0 otherwise
                box_opened = 0 ! all boxes are closed at the start
                tot_closed = num_boxes
                Do j = 1, num_boxes
                    closed_boxes_queue(j) = j
                Enddo

                ! Henceforth j will denote the box chosen by the player

                ! Choose the initial box    
                ! If the strategy is 'randomclosed' or 'random', initial condition is forced to be random
                if (initial_choice == 'random' .or. strategy == 'randomclosed' .or. strategy == 'random') then
                    ! The initial box is chosen uniformly randomly
                    j = rand_int(int(1, 8), num_boxes)
                elseif (initial_choice == 'natural') then
                    ! The player i starts with box i
                    j = i 
                elseif (initial_choice == 'offset') then
                    ! Start from the box i + offset
                    j = next_box(i, num_boxes, offset)
                endif

                attempts = 1
                box_opened(j) = 1
                ! Overwrite the current entry by the label of the last closed box 
                closed_boxes_queue(j) = closed_boxes_queue(tot_closed)
                tot_closed = tot_closed - 1
                tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                IF (strategy == 'key' .or. strategy == 'box') THEN

                    Do while (boxes(j) /= i .and. attempts < max_attempts)
                        ! Choose the next j
                        If (boxes(j) == -1 .and. strategy == 'key') then
                            ! The box is empty, decide what to do 
                            iF (escape_strategy == 'randomclosed')then
                                ! Choose a random box among the boxes not 
                                ! opened so far. k is the index of the box 
                                ! in the queue
                                k = rand_int(int(1, 8), tot_closed) 
                                j = closed_boxes_queue(k)
                                if (k < tot_closed) then
                                    ! Overwrite the current entry by the label
                                    ! of the last closed box 
                                    closed_boxes_queue(k) = &
                                      & closed_boxes_queue(tot_closed)
                                endIF
                            elseiF (escape_strategy == 'boxclosed')then
                                ! Choose the next non-empty box in the sequence 
                                j = next_box(j, num_boxes, int(1,8))
                                do while (box_opened(j) == 1)
                                    j = next_box(j, num_boxes, int(1,8))
                                enddo
                            endiF
                        Else
                            ! The box is not empty or strategy is other than 'key'
                            call random_number(r)
                            If (r > switch_prob) then
                                iF (strategy == 'key') then
                                    ! Choose the box with number of the key found 
                                    ! in the current box
                                    j = boxes(j)
                                elseiF (strategy == 'box') then
                                    ! Choose the next box
                                    j = next_box(j, num_boxes, incr)
                                endif  
                            Endif
                        Endif

                        ! Check whether new chosen box is already open
                        If (box_opened(j) == 1 .or. r < switch_prob)then
                            ! The box j is already open, decide what to do
                            if (escape_strategy == 'randomclosed') then
                                ! so choose a random box among the boxes not 
                                ! opened so far. k is the index of the box in 
                                ! the queue
                                k = rand_int(int(1, 8), tot_closed) 
                                j = closed_boxes_queue(k)
                            elseif (escape_strategy == 'boxclosed') then
                                ! Choose the next closed box in the sequence 
                                j = next_box(j, num_boxes, int(1,8))
                                do while (box_opened(j) == 1)
                                    j = next_box(j, num_boxes, int(1,8))
                                enddo
                            endif
                        Endif
                        box_opened(j) = 1
                        ! overwrite element j in the closed_boxes_queue with the last element
                        do k = 1, num_boxes
                            if (closed_boxes_queue(k) == j)then
                                closed_boxes_queue(k) = closed_boxes_queue(tot_closed)
                                exit
                            endif
                        enddo
                        tot_closed = tot_closed - 1
                        attempts = attempts + 1
                        tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                    Enddo

                ELSEIF (strategy == 'randomclosed') then
                    Do while (boxes(j) /= i .and. attempts < max_attempts)
                        ! Choose a random box among the boxes not opened so far.
                        ! k is the index of the box in the queue
                        k = rand_int(int(1, 8), tot_closed) 
                        j = closed_boxes_queue(k)
                        ! Overwrite the current entry by the label of the last closed box
                        closed_boxes_queue(k) = closed_boxes_queue(tot_closed)
                        ! Since box j is opened, decrease the number of closed boxes
                        tot_closed = tot_closed - 1
                        attempts = attempts + 1
                        tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                    Enddo 
                ELSEIF (strategy == 'random') then
                    ! Pure random strategy where the player can even repeatedly 
                    ! open the same box again and again
                    Do while (boxes(j) /= i .and. attempts < max_attempts)
                        j = rand_int(int(1, 8), num_boxes)
                        attempts = attempts + 1
                        tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                    Enddo
                ENDIF
                ! Note whether player i succeeded in finding his key or not
                IF (boxes(j) == i) success(i) = 1
            ENDDO ! Loop over players
            ! To get the total number of winners, we simply add all the elements
            ! of array "success" because its i'th entry is 1 if player i wins and
            ! is zero otherwise
            tot_winners = sum(success)
            pmf(tot_winners) = pmf(tot_winners) + 1
        ENDDO
    end subroutine play_game
    !================================================================
    
    ! Return a random integer between a and b (inclusive) randomly
    function rand_int(a, b)
        implicit none
        integer*8 :: a, b, rand_int
        real*8 :: r
        
        call random_number(r)
        rand_int = a + Floor((b-a+1)*r)
    end function rand_int
    !================================================================

    function next_box(j, num_boxes, incr)
        implicit none
        integer*8 :: j, num_boxes, incr, next_box
        
        next_box = mod(j+incr, num_boxes)
        if (next_box == 0)then
            next_box = num_boxes
        endif
    end function
    !================================================================

    ! random permutation
    subroutine random_permute(n, x)
        ! Algorithm taken from Knuth, vol2
        implicit none
        integer*8 :: i, j, temp, n, x(n)
    
        DO j = n, 1, -1
            i = rand_int(int(1, 8), j)
            temp = x(i)
            x(i) = x(j)
            x(j) = temp
        ENDDO
    end subroutine random_permute
    !================================================================
End module strategies
