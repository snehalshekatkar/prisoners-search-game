#    This file is part of the package dependency-networks
#
#    Copyright (C) 2020 Snehal Madhukar Shekatkar <snehal@inferred.co>
#    Copyright (C) 2020 Ivano Lodato <ivano.lodato@gmail.com>
#
#    This package is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    The package is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this package. If not, see <https://www.gnu.org/licenses/>.

import gs
import adi
import routines

def play_game(num_boxes=100, num_players=100, max_attempts=50,
    tot_permutations=100,  strategy='key', escape_strategy='randomclosed', 
    increment=1, initial_choice='natural', switch_prob=0., offset=0, 
    be_stupid=False):

    r"""Play the prisoners' game using a given strategy, and return the 
    Probability Mass Function (PMF) for the number of winners as the 
    maximum number of attempts is varied. 

    Parameters
    ----------
    num_boxes : int (optional, default: ``100``)
        The total number boxes.
    num_players : int (optional, default: ``100``)
        The total number of players. This has to be less than or equal 
        to num_boxes. If ``num_players < num_boxes``, only ``num_players`` 
        number of boxes will be occupied and others are kept empty.
    max_attempts : int (optional, default: ``50``)
        Maximum number of attempts each prisoner is allowed to find the key. 
        In the classical case, this is set to the half the number of boxes.
    tot_permutations : int (optional, default: ``100``)
        The total number of times the game is played. Each time the permutation
        of keys inside the boxes is randomly generated. 
    strategy : ``str`` (optional, default: ``"key"``)
        Strategy to be used when the box is non-empty. This must be one of
        ``"key"``, ``"box"``, ``randomclosed``, ``"random"``, ``ADI`` 
        or ``goyalsaks``.  For ``random_closed``, each time the random choice 
        is made only among the boxes that are closed.
    escpae_strategy: ``str`` (optional, default: ``"randomclosed"``)
        Strategy to be used when the box is either empty or already visited.
        This must be either ``"randomclosed"`` or ``"boxclosed"``. 
        When ``"randomclosed"`` is used, whenever the chosen box is already 
        open, or when the opened box is empty, next box is chosen uniformly 
        randomly among the closed boxes. 
        When ``"boxclosed"`` is used, in the above situation, next box is 
        chosen to be the first closed box in the sequence starting from the 
        current box.
    increment: int (optional, default: ``1``)
        Increment to be used for the box strategy. This is ignored if the 
        strategy being used in not the "box strategy".
    initial_choice: ``str`` (optional, default: ``"natural"``)
        Choice of the initial box. This must be one of ``"natural"``, 
        ``"offset"`` or ``"random"``. Here ``"natural"`` means the player ``i``
        starts from box ``i``, ``"offset"`` means the player ``i`` starts from
        box ``i + offset`` (modulo ``num_boxes``). If this is ``"random"``, 
        each player chooses the initial box uniformly randomly. This has no 
        effect if the strategy is ``randomclosed`` or ``random``, and the 
        initial choice in that case is always set to ``random``. 
    be_stupid: bool (optional, default: ``False``)
        Whether to be stupid and continue with the strategy even when the box
        is already open. 
    switch_prob: float (optional, default: ``0.``)
        Probability to randomly switch to the escape_strategy even when not 
        required.
    offset: int (optional, default: ``0``)
        Offset to be used to choose the initial box when the ``initial_choice``
        is ``offset``. ``offset = 0`` is equivalent to the ``natural`` for 
        ``iniitial_choice``. 

    Returns
    -------
    tot_used_attempts : :class:`numpy.ndarray`
        This will be an array containing the total number of attempts that 
        were used by players in each round. 
    pmf : :class:`numpy.ndarray`
        This will be an unnormalized histogram containing the frequencies of
        the total number of winners for ``tot_permutations`` number of games.
        
    """
    if not isinstance(num_boxes, int):
        raise TypeError('`num_boxes` must be of type int')
    if not isinstance(num_players, int):
        raise TypeError('`num_players` must be of type int')
    if num_players > num_boxes:
        raise ValueError("number of players can't be greater than number " 
            "of boxes")
    if not isinstance(max_attempts, int):
        raise TypeError('`max_attempts` must be of type int')
    if max_attempts < 1:
        raise ValueError('`max_attempts` should be at least 1')
    if not isinstance(tot_permutations, int):
        raise TypeError('`tot_permutations` must be of type int')
    if not isinstance(strategy, str):
        raise TypeError("`strategy` must be of type `str`")
    if strategy not in ['key', 'box', 'randomclosed', 'goyalsaks', 'random', \
        'ADI']:
        raise ValueError("strategy must be one of 'key', 'box', "
        "'randomclosed', 'goyalsaks', 'ADI' or 'random'")
    if escape_strategy not in ['randomclosed', 'boxclosed', 'NA']:
        raise ValueError("`escape_strategy` must be either "
        "'randomclosed' or 'boxclosed'")
    if escape_strategy == 'NA':
        escape_strategy = 'randomclosed'
    if not isinstance(increment, int):
        raise TypeError('`increment` must be of type int')
    if initial_choice not in ['natural', 'offset', 'random']:
        raise ValueError("`initial_choice` must be one of 'natural', "
        "'offset' or 'random'")
    if not isinstance(be_stupid, bool):
        raise TypeError("`be_stupid` must be of type `bool`")
    if not isinstance(offset, int):
        raise TypeError('`offset` must be of type int')
    if not 0. <= switch_prob <= 1.0:
        raise ValueError("swithc_prob must be between 0 and 1")

    if strategy == 'ADI':
        tot_used_attempts, pmf = adi.adi.play_game(num_boxes, num_players, 
            max_attempts, tot_permutations)
    elif strategy == 'goyalsaks':
        tot_used_attempts, pmf = gs.gs.play_game(num_boxes, num_players, 
            max_attempts, tot_permutations)
    else:
        tot_used_attempts, pmf = routines.strategies.play_game(num_boxes, 
            num_players, max_attempts, offset, increment, strategy, 
            escape_strategy, initial_choice, switch_prob, int(be_stupid), 
            tot_permutations)
    return tot_used_attempts, pmf
