# Copyright (C) 2020 Snehal M. Shekatkar <snehal@inferred.co>
# Copyright (C) 2020 Ivano Lodato <ivano.lodato@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
prisoners-search-game
==================

An efficient python package to study prisoners search games

The docstring examples assume that `prisoners_search_game` has been imported as `psg`::

"""
__version__ = '1.0.0'
__author__ = 'Snehal M. Shekatkar <snehal@inferred.co>, Ivano Lodato <ivano.lodato@gmail.com>'
__copyright__ =  'Copyright 2020 Snehal M. Shekatkar, Ivano Lodato'
__license__ = 'GPL v3 or above'

from .play import play_game
from .auxiliary import *
