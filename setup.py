from setuptools import find_packages
from numpy.distutils.core import setup, Extension

ext = [Extension(name='gs', sources=['src/fortran/gs.f95']), 
       Extension(name='adi', sources=['src/fortran/adi.f95']), 
       Extension(name='routines', sources=['src/fortran/routines.f95']), 
        ]
setup(
    name='prisoners-search-game',
    version='1.0.0',
    author='Snehal M. Shekatkar, Ivano Lodato',
    author_email='snehal@inferred.co, ivano.lodato@gmail.com',
    long_description=open('README.md').read(),
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    install_requires=['numpy'],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3', 
        'Programming Language :: Python :: 3.6', 
    ],
    ext_modules=ext, 
    python_requires='>=3.6', 
)

