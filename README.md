prisoners-search-game -- a python package to study games involving prisoners search
=================================================================================

prisoners-search-game is a python package that can be used to study prisoners
search games i.e. the fictitious games in which a set of players try to search
through a large collection of boxes. A player wins if she can find a
pre-decided key within a given number of attempts. Perhaps the most famous
prisoners-search-game is the classical "100 prisoners problem" proposed first by
Gal and Miltersen. On an abstract level various strategies proposed to play the
game aim to answer fundamental problems about time and space complexity of
various data structures and retrievals.

The package implements several strategies to study the problem under various 
settings including Skyum, Goyal-Saks, Avis-Devroye-Iwama, as well as many
hybrid strategies.

The package is written in python and Fortran to take advantage of both the
languages. Because of this, the end user can use it as if it were a pure python
package, although crucial calculations are performed under-the-hood in Fortran.

prisoners-search-game is free software, you can redistribute it and/or modify it
under the terms of the GNU General Public License, version 3 or above. See
COPYING for details.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

INSTALLATION
------------
Internally, prisoners-search-game uses numpy, and so numpy needs to be
installed. Moreover, since the core of the package is written in fortran, a
fortran compiler (e.g. gfortran) is needed when. For the same reason, you also
need to have setuptools installed.  Finally, in case you wish to plot the
results, matplotlib also needs to be installed.

To install the package, follow the instructions below:

1. Download or git-clone this repository to your machine

   git clone https://gitlab.com/snehalshekatkar/prisoners-search-game.git

2. Now, cd to the cloned directory:

    cd prisoners-search-game

    and run the following command in the terminal:

    sudo python3 -m pip install .

Now the package can be imported in python scripts and the interpreter:

    >>> import prisoners_search_game as psg
    >>> psg.play_game()

--
Snehal M. Shekatkar <snehal@inferred.co>
Ivano Lodato <ivano.lodato@gmail.com>
